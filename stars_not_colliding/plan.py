from typing import List, Hashable, Optional, Sequence, Union
from networkx import Graph

class Agent:
    """Agent class which should serve as a base class for library users wishing
    to use `a_star_w` algorithm."""

    def __init__(self, idx: Hashable, position, target):
        self.idx = idx
        self.position = position
        self.target = target
        self.plan: Optional[Plan] = None

    def __repr__(self) -> str:
        return "Agent " + str(self.idx) + " positioned at: " + str(self.position) + ", travelling to: " + str(
            self.target)

    def __hash__(self) -> int:
        return hash(self.idx)

    def id(self):
        return self.idx

    def set_plan(self, plan):
        self.plan = plan

def targets_reached(agents: Sequence[Agent]):
    for a in agents:
        if a.target != a.position:
            return False
    return True

class Node:
    """Node class used internally by `a_star_w`.
    
    ### Node in graphs

    It is recommended to use this wrapper class with functions of the library.
    Any hashable can be used as a base of the Node which is also true for nodes
    of a `networkx.Graph`. Any `networkx.Graph` can be changed to use the
    wrapper with invocation of `graph.from_networkx_to_nodes`. All other graph
    generators of submodule `graph` use this wrapper class.

    ### Node and agents

    The Node provides an additional field which can be used to track the
    location of agents. Use `set_agent` to change its value. The interpretation
    the field is left to library users.
    """

    def __init__(self, idx: Hashable):
        self.idx = idx
        self.agent: Optional[Agent] = None

    def __hash__(self):
        return hash(self.idx)

    def __repr__(self) -> str:
        return "Node:" + str(self.idx)

    def __eq__(self, other):
        if isinstance(other, Node):
            return self.idx == other.idx
        else:
            return self.idx == other

    def __lt__(self, other):
        return self.idx < other

    def __gt__(self, other):
        return self.idx > other

    def __len__(self):
        return len(self.idx)

    def __getitem__(self, i):
        return self.idx[i]

    def __iter__(self):
        return iter(self.idx)

    def id(self):
        return self.idx

    def set_agent(self, agent: Optional[Agent]):
        self.agent = agent
        if agent:
            agent.position = self


Path = List[Node]

class Plan:
    """Core class of the `a_star_w` library.
    
    Plan is the data structure returned by an `a_star_w` algorithm function. It
    consists of a path generated by an A* algorithm which itself assumes no
    deviations and a list of deviation domains which are graphs labelled with
    wavefront expansion algorithm. To use the Plan invoke `next_step` - the
    method will recommend the next node to which an agent should move. If the
    agent left the path because of a collision with an other agent it should
    enter a deviation domain for which the method is still valid.
    """
    def __init__(self, idx: Agent, a_path: Path, deviation_domains: List[Graph]):
        self.idx = idx
        self.a_path = a_path  # NOTE: Could use a better name
        self.deviation_domains = deviation_domains

    def __repr__(self) -> str:
        return "Plan with path: " + str(self.a_path) + " and " + str(len(self.deviation_domains)) + " d. domains"

    def id(self) -> Agent:
        return self.idx

    def path(self) -> Path:
        """An A* generated path that is the base of the plan."""
        return self.a_path

    def deviation_domain(self, position: Node) -> Graph:
        """Returns a deviation domain to which a specific point (position) belongs."""
        for d in self.deviation_domains:
            if position in d:
                return d

    def next_step(self, from_pos: Node) -> Union[Node, None]:
        """Find the next step that should be taken from a given position
        according to the plan.
    
        ### Returns
        The method returns None when goal was reached otherwise it returns the
        next node to which an agent should move.
        
        ### Errors
        A RuntimeError is raised when agent asks a for a step from a position
        not included in the Plan.
        ---
        More information can be found in the documentation of `Plan` class.
        """
        if from_pos in self.a_path:
            if from_pos == self.a_path[-1]:
                # We are already at the end of the path
                return None
            i = self.a_path.index(from_pos)
            return self.a_path[i + 1]
        else:
            D = self.deviation_domain(from_pos)
            if not D:
                raise RuntimeError
            # use wavefront data stored in the deviation domain
            neigh = list(D.neighbors(from_pos))
            return min(neigh)


class WavefrontLabelled:
    """Class used internall by `a_star_w` within wavefront expansion. It is not
    meant to be used by a library user."""

    def __init__(self, node, cost) -> None:
        self.node = node
        self.cost = cost

    def __hash__(self) -> int:
        return hash(self.node)

    def __eq__(self, __o: object) -> bool:
        if isinstance(__o, WavefrontLabelled):
            return (self.node == __o.node and self.cost == __o.cost)
        else:
            return self.node == __o

    def __lt__(self, __o: object) -> bool:
        if isinstance(__o, WavefrontLabelled):
            return self.cost < __o.cost
        else:
            return self.node < __o

    def __gt__(self, __o: object) -> bool:
        if isinstance(__o, WavefrontLabelled):
            return self.cost > __o.cost
        else:
            return self.node > __o

    def __repr__(self) -> str:
        return "{" + str(self.node) + ": " + str(self.cost) + "}"

    def cost(self):
        return self.cost

    def node(self):
        return self.node
