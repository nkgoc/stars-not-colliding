from typing import Dict, Mapping, Sequence, List, Set, Tuple

import networkx as nx
from networkx import Graph
import networkx

from stars_not_colliding.plan import Agent, Plan, Node, WavefrontLabelled

Path = List[Node]


def a_star_w(G: Graph, agents: Sequence[Agent], heuristic=None, weight='weight', g: int = 2) -> Dict[Agent, Plan]:
    """Performs an A * W hybrid planning algorithm for a given graph and a list of
    agents.
    
    Creates a plan for each agent assuming all agents exist within the
    same graph. It can operate on any subclass of `networkx.Graph`. The
    algorithm offers a compromise between wavefront's robustness and low
    complexity of A*. 
    """

    # A* for all agents
    paths: Dict[Agent, Path] = dict()
    for a in agents:
        paths[a] = a_star(G, a, heuristic, weight)

    # check collisions
    collision_points = check_for_collisions(G, paths)

    # grow deviation domain
    domains: Dict[Agent, List[Graph]] = dict()
    for a_list in collision_points.values():
        for a in a_list:
            domains[a] = list()
    for cp in collision_points.keys():
        for a in collision_points[cp]:
            domains[a].append(generate_sub_space(G, cp, g))

    # calculate Wavefront and modify the domain
    for a, d_list in domains.items():
        for i in range(len(d_list)):
            d = domains[a][i]
            domains[a][i] = wavefront(d, G, paths[a], heuristic, weight)

    # at this point we should have:
    #   * paths[Agent, Path]
    #   * domains[Agent, List[Graph]]
    # now a plan should be generated

    plans: Dict[Agent, Plan] = dict()

    for a in agents:
        path = paths[a]
        try:
            domain = domains[a]
            plans[a] = Plan(a, path, domain)
            a.set_plan(plans[a])
        except KeyError:
            plans[a] = Plan(a, path, list())
            a.set_plan(plans[a])

    # Put agents onto the graph at their start positions
    if issubclass(Node, list(G)[0].__class__):
        for a in agents:
            a.position.set_agent(a)

    return plans


def a_star(G: Graph, agent: Agent, heurestic=None, weight='weight') -> Path:
    """Wrapper of `networkx.astar_path` used in `a_star_w`"""
    astar_path = nx.astar_path(G, agent.position, agent.target, heurestic)
    path = list()
    for n in astar_path:
        path.append(n)
    return path


def wavefront(D: Graph, G: Graph, path: Path, heuristic=None, weight='weight'):
    """Wavefront expansion algorithm which relabes nodes of devation domain with
    cost. Used in `a_star_w`.

    Performs the wavefront expansion relabeling the nodes within the domain so
    that they contain the information about the cost of reaching the goal
    (closest next point on path that is outside the domain). Since the goal does
    not exist within the deviation domain (in most cases) the access the the
    original graph is supplemented with `G` argument, while the domain is passed
    as `D`.
    """
    search_path = path.copy()

    # search for a target scanning the path, starting from last element, for a
    # node closest to analyzed deviation domain. Note the node that was before
    # our match
    dest = search_path.pop()
    prev = dest
    while search_path and dest not in D.nodes:
        prev = dest
        dest = search_path.pop()

    labelled = dict()

    # If `prev != dest` (prev equal dest implies that the goal is within the
    # domain) Start with neighbours of prev that exist within the domain,
    # otherwise start with dest.
    if prev != dest:
        neigh = set(D.nodes).intersection({n for n in G.neighbors(prev)})
    else:
        neigh = D.neighbors(dest)
        labelled[dest] = 0.0

    to_visit = list()  # BFS queue

    # Start expansion with initial set. This part can be done in more performant
    # manner and thus is separated from normal BFS algorithm.
    for n in neigh:
        # Use the heuristic function, if it is available, to calculate costs.
        if heuristic:
            cost_label = heuristic(prev, n)
        else:
            cost_label = G[prev][n][weight]
        # Relabel nodes to cost
        labelled[n] = cost_label
        to_visit.append(n)

    # BFS in two versions
    if not heuristic:
        while to_visit:
            currently_inspected = to_visit.pop()
            for n in D.neighbors(currently_inspected):
                if n not in labelled:
                    cost_label = G[currently_inspected][n][weight] + labelled[currently_inspected]
                    labelled[n] = cost_label
                    to_visit.append(n)
                if labelled[n] > G[currently_inspected][n][weight] + labelled[currently_inspected]:
                    labelled[n] = G[currently_inspected][n][weight] + labelled[currently_inspected]
    else:
        while to_visit:
            currently_inspected = to_visit.pop()
            for n in D.neighbors(currently_inspected):
                if n not in labelled:
                    cost_label = heuristic(prev, n)
                    labelled[n] = cost_label
                    to_visit.append(n)

    mapping = dict()
    for k, v in labelled.items():  # k = node, v = cost
        mapping[k] = WavefrontLabelled(k, v)

    D = nx.relabel_nodes(D, mapping, copy=True)
    return D


def check_for_collisions(G: Graph, paths: Dict[Agent, Path]) -> Dict[Node, List[Agent]]:
    """
    Find among paths collision points.
    """

    collisions: Dict[Node, List[Agent]] = dict()

    for a, p in paths.items():
        for n in p:
            try:
                collisions[n].append(a)
            except KeyError:
                collisions[n] = list()
                collisions[n].append(a)

    collisions = dict(filter(lambda x: len(x[1]) > 1, collisions.items()))

    return collisions


def generate_sub_space(G: Graph, collision_point, g: int = 2) -> Graph:
    """
    Generate a deviation sub-space for a given collision point using a growth
    factor g.
    """
    sub_space_nodes = set()
    sub_space_nodes.add(collision_point)

    queue = list()
    queue.append(collision_point)

    while queue and g:
        level_size = len(queue)
        while level_size:
            level_size -= 1
            currently_inspected = queue.pop()
            neighbors = {n for n in G.neighbors(currently_inspected)}
            queue.extend(neighbors)
            sub_space_nodes = sub_space_nodes.union(neighbors)
        g -= 1

    # # Create a subgraph
    # sub_space = G.__class__()
    # sub_space.add_nodes_from((n, G.nodes[n]) for n in sub_space_nodes)
    # if sub_space.is_multigraph():
    #     sub_space.add_edges_from((n, nbr, key, d)
    #         for n, nbrs in G.adj.items() if n in sub_space_nodes
    #         for nbr, keydict in nbrs.items() if nbr in sub_space_nodes
    #         for key, d in keydict.items())
    # else:
    #     sub_space.add_edges_from((n, nbr, d)
    #         for n, nbrs in G.adj.items() if n in sub_space_nodes
    #         for nbr, d in nbrs.items() if nbr in sub_space_nodes)
    # sub_space.graph.update(G.graph)
    sub_space = G.subgraph(sub_space_nodes).copy()

    return sub_space


def euclidean_distance(a, b) -> float:
    """
    A simple euclidean distance heuristic. It assumes that nodes can be
    decomposed into a tuple of coordinates. Otherwise it throws IndexError.
    """
    if len(a) < 2 or len(b) < 2:
        raise IndexError
    (x1, y1) = a
    (x2, y2) = b
    return ((x1 - x2) ** 2 + (y1 - y2) ** 2) ** 0.5
