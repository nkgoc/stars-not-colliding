# Stars Not Colliding

"Stars Not Colliding" is a library implementing a hybrid A * W planning
algorithm. The implementation is supplied as `a_star_w` function which operates
on graphs and agents. The description of the algorithm can be found in the
following papers:
- Wojnicki I., Ernst S., Turek W. (2015) [A Robust Heuristic for the
  Multidimensional A-star/Wavefront Hybrid Planning
  Algorithm](https://doi.org/10.1007/978-3-319-19369-4_26)
- Wojnicki I., Ernst S. (2017) [Defining Deviation Sub-spaces for the A*W Robust
  Planning Algorithm](https://doi.org/10.1007/978-3-319-54472-4_37)

## Elements of the library

The library consists of three modules:
- `a_star_w` - contains the `a_star_w` function which is the core of the library
- `plan` - contains necessary data structures such as: `Node`, `Agent`, `Plan`
- `graph` - contains functions which are meant to simplify the use of graphs in
  the context of library and provide a bridge between the library and its
  dependency [`networkx`](https://networkx.org/)

## Features of the library

The library makes no assumptions on the intended use of the Plan and
characteristics of agents. The provided functionality is minimal. It is up to
the user to e.g. create simulation of moving agents.

In the library included are:
- A * W algorithm implementation which works on any subclass of
  `networkx.Graph`.
- A base class for agents called `Agent`.
- A `Plan` data structure which tells a given agent how to get from node A to B
  even in the case of deviating from the primary path to avoid a collision with
  another agent.
- Example heuristics for the algorithm.
- A wrapper `Node` which has an extra field making it possible to store agents
  inside

The library does not include:
- Any behavioural functionality for the agents (see the examples section to find
  out how to implement such functionality)


## Examples

### The most minimal example

```python
# G is the graph on which agent A1 will be moving
G = grid_graph_with_diagonals(7, 7)
# To create an agent supplied must be: id, start position, goal position
A1 = Agent(1, Node((0, 0)), Node((6, 6)))
# The algorithm returns a dictionary of plans. `p` is the plan for agent A1
p = a_star_w(G, [A1])[A1]

# The following gives a node which should be reached next assuming current 
# position is the supplied argument.
p.next_step(Node((0, 0)))
```

### Extending the library's functionality

The following example can be found and run in [`docs/example.py`](docs/example.py).

```python
# The base Agent class provides only as much data as it is needed to run
# the A*W algorithm so it is recommended to extend the class to provide any
# behavioral functionality. Here to be able to move on the graph agent gets
# an access to the graph at the moment of creation.
class MovingAgent(Agent):
    def __init__(self, idx: Hashable, position: Node, target: Node, graph: Graph):
        super().__init__(idx, position, target)
        self.graph = graph
        
    def avoid_collision(self, on_point: Node) -> Optional[Node]:
        neigh = self.graph[on_point]
        n = neigh.next()
        try:
            while n.agent:
                n.next()
        except StopIteration:
            return None
        return n
    
    def move(self):
        next_step = self.plan.next_step(self.position)
        if not next_step:
            return
        if next_step.agent:
            next_step = self.avoid_collision(next_step)
            if next_step:
                self.position.set_agent(None)
                next_step.set_agent(self)
            else:
                return
        else:
            self.position.set_agent(None)
            next_step.set_agent(self)

# A simple matrix representation can be used to create grid-based maps
# with obstacles in visual way
m = [[0, -1, -1,  0],
     [0, -1,  0,  0],
     [0,  0,  0, -1],
     [0,  0,  0,  0]]

G = simple_graph_from_matrix(m, diagonals=True)
A1 = MovingAgent(1, Node((0,0)), Node((0,3)), G)
A2 = MovingAgent(2, Node((0,3)), Node((0,0)), G)
A3 = MovingAgent(3, Node((3,3)), Node((2,0)), G)
agents = [A1, A2, A3]
P = a_star_w(G, agents)

# A simple simulation loop:
i = 0
while(not targets_reached(agents)):
    i += 1
    for a in agents:
        a.move()
        print(i, a)
```
